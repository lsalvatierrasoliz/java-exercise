package com.company;

import java.util.Objects;

public class Classroom {

    public String name;
    public final Localization localization;


    public Classroom(String name,  Localization localization) {
        this.name = name;
        this.localization = localization;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Localization getLocalization() {
        return localization;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, localization);
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;

        if(obj == null) return false;
        if(getClass() != obj.getClass()) return false;

        Classroom that = (Classroom) obj;
        if(Objects.equals(that.name, this.name) &&
                Objects.equals(that.localization, this.localization)) return true;

        return false;
    }
}
