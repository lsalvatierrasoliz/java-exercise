package com.company;

public class SquareRangeLocation implements RangeLocation {

    private final double size;
    private final Localization localization;

    private final double latitudeDiff;
    private final double longitudeDiff;

    private final double latitudeLimitInferior;
    private final double latitudeLimitSuperior;

    private final double longitudeLimitInferior;
    private final double longitudeLimitSuperior;


    public SquareRangeLocation(double squareSize, Localization sourceLocalization) {
        this.size = squareSize;
        this.localization = sourceLocalization;

        // latitude difference equivalent to half of size square. This formula is to calculate distance between two points when longitude points has the same value.
        this.latitudeDiff = (size / 2) / KM_PER_DEGREE;
        // longitude difference equivalent to half of size square. This formula is to calculate distance between two points when latitude points has the same value.
        this.longitudeDiff = (size / 2) / (KM_PER_DEGREE * Math.abs(Math.cos(localization.getLatitude())));

        // limit inferior and limit superior for latitude and longitude coordinate from central localization of class room.
        this.latitudeLimitInferior =  localization.getLatitude() - latitudeDiff;
        this.latitudeLimitSuperior = localization.getLatitude() + latitudeDiff;
        this.longitudeLimitInferior = localization.getLongitude() - longitudeDiff;
        this.longitudeLimitSuperior =  localization.getLongitude() + longitudeDiff;
    }

    /**
     * Evaluate if a target localization is within a range of values for latitude and longitude.
     *
     * @param targetLocalization
     * @return
     */
    @Override
    public Boolean isInRange(Localization targetLocalization) {

        if(targetLocalization.getLatitude() >= latitudeLimitInferior &&
                targetLocalization.getLatitude() <= latitudeLimitSuperior &&
                targetLocalization.getLongitude() >= longitudeLimitInferior &&
                targetLocalization.getLongitude() <= longitudeLimitSuperior){


            return Boolean.TRUE;

        }

        return Boolean.FALSE;
    }
}
