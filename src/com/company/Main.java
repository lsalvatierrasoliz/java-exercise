package com.company;

import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {

        Classroom engineering_classroom = new Classroom("Principles of computational geo-location analysis", new Localization(34.069140, -118.442689));
        Classroom geology_classroom = new Classroom("Sedimentary Petrology",new Localization(34.069585, -118.441878));
        Classroom psychology_classroom  = new Classroom("Introductory Psychobiology",new Localization(34.069742, -118.441312));
        Classroom music_classroom   = new Classroom("Art of Listening",new Localization(34.070223, -118.440193));
        Classroom humanities_classroom   = new Classroom("Art Hitory",new Localization(34.071528, -118.441211));


        Student john_student = new Student("John Wilson", new Localization(34.069149, -118.442639));
        Student jane_student  = new Student("Jane Graham", new Localization(34.069601, -118.441862));
        Student pam_student  = new Student("Pam Bam", new Localization( 34.071513, -118.441181));

        Set<Classroom> classrooms = new HashSet<>();
        classrooms.add(engineering_classroom);
        classrooms.add(geology_classroom);
        classrooms.add(psychology_classroom);
        classrooms.add(music_classroom);
        classrooms.add(humanities_classroom);


        Set<Student> students = new HashSet<>();
        students.add(john_student);
        students.add(jane_student);
        students.add(pam_student);


        Main main = new Main();
        main.studentsInClasses(students, classrooms, 0.02).forEach(System.out::println);

    }

    /**
     * Find students that are within range of a classroom location
     * @param students
     * @param classroomList
     * @param squareSize
     * @return list of students
     */
    public Set<Student> studentsInClasses(Set<Student> students, Set<Classroom> classroomList, final double squareSize){

        Set<Student> resultList = new HashSet<>();

        for(Classroom cr : classroomList){
            RangeLocation squareRangeLocation = new SquareRangeLocation(squareSize, cr.getLocalization());

            // if there is no more students to check
            if(students.size() == 0) break;

            // don't iterate students that were already found it in a classroom
            students.removeAll(resultList);

            for(Student student : students){
                if(squareRangeLocation.isInRange(student.getLocalization())){
                    resultList.add(student);
                }

            }
        }

        return resultList;
    }
}
