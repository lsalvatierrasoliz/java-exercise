package com.company;

public interface RangeLocation {

    double KM_PER_DEGREE = 111.2;
    Boolean isInRange(Localization targetLocalization);
}
