package com.company;

import java.util.Objects;

public class Localization {

    private final double latitude;
    private final double longitude;

    public Localization(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;

        if(obj == null) return false;
        if(getClass() != obj.getClass()) return false;

        Localization that = (Localization) obj;
        if(Objects.equals(that.latitude, this.latitude) &&
            Objects.equals(that.longitude, this.longitude)) return true;

        return false;

    }

    @Override
    public String toString() {
        return "Localization{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
