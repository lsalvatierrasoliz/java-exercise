package test;


import com.company.Classroom;
import com.company.Localization;
import com.company.Main;
import com.company.Student;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MainTest {

    @Test
    public void testFunctionStudentsInCourse(){

        // write your code here

        Classroom engineering_classroom = new Classroom("Principles of computational geo-location analysis", new Localization(34.069140, -118.442689));
        Classroom geology_classroom = new Classroom("Sedimentary Petrology",new Localization(34.069585, -118.441878));
        Classroom psychology_classroom  = new Classroom("Introductory Psychobiology",new Localization(34.069742, -118.441312));
        Classroom music_classroom   = new Classroom("Art of Listening",new Localization(34.070223, -118.440193));
        Classroom humanities_classroom   = new Classroom("Art Hitory",new Localization(34.071528, -118.441211));


        Student john_student = new Student("John Wilson", new Localization(34.069149, -118.442639));
        Student jane_student  = new Student("Jane Graham", new Localization(34.069601, -118.441862));
        Student pam_student  = new Student("Pam Bam", new Localization( 34.071513, -118.441181));

        Set<Classroom> classrooms = new HashSet<>();
        classrooms.add(engineering_classroom);
        classrooms.add(geology_classroom);
        classrooms.add(psychology_classroom);
        classrooms.add(music_classroom);
        classrooms.add(humanities_classroom);


        Set<Student> students = new HashSet<>();
        students.add(john_student);
        students.add(jane_student);
        students.add(pam_student);

        final Integer sizeInputStudents = students.size();

        Main main = new Main();
        Set<Student> result =  main.studentsInClasses(students, classrooms, 0.02);

        assertThat(sizeInputStudents, is(result.size()));

    }
}
