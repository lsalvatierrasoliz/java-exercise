# java-exercise
Given each student has a geolocation lat/lon point, how would you determine which students are physically in any classroom?  

Write a function that returns the students if they are in a classroom.  


Assumptions
 
Each classroom has a square shape of 20m X 20m and none of the classrooms intersect.
Students are dimensionless outside of their latitude / longitude point
Height is not a concern for either the student or the classroom
It doesn’t matter which student was in which classroom, we only care about the list of students found
This is intended to be performed in memory where you don’t have the usage of a database of some sort. 

NOTE: If you made any other assumptions please write those down.

## CLASSES

### RangeLocation
`Boolean isInRange(Localization loc);`
Interface contract for any kind of shape
 
### SquareRangeLocation
Specific implementation for Square shape Range Location. This class is able to 
evaluate if some point localization is within a range of square range.
 
### Student
Model template for student.
 
### ClassRoom
Model template for classroom.

### Localization
Model template for latitude and longitude points.

### Main
`Set<Student> studentsInClasses(Set<Student> students, Set<Classroom> classroomList, final double squareSize)`

Main class as entry point for main function to determine which students from a list are in any classroom.

### test/MainTest

Simple test main function "studentsInClasses"


  






