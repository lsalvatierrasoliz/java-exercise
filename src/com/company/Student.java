package com.company;

import java.util.Objects;

public class Student {

    private String name;
    private Localization localization;

    public Student(String name, Localization localization) {
        this.name = name;
        this.localization = localization;
    }

    public Student(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Localization getLocalization() {
        return localization;
    }

    public void setLocalization(Localization localization) {
        this.localization = localization;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, localization);
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;

        if(obj == null) return false;
        if(getClass() != obj.getClass()) return false;

        Student that = (Student) obj;
        if(Objects.equals(that.name, this.name) &&
                Objects.equals(that.localization, this.localization)) return true;

        return false;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", localization=" + localization +
                '}';
    }
}
